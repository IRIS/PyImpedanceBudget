# PyImpedanceBudget
![FCC project logo](https://impedance.web.cern.ch/impedance/fcchh/images/fcc_logo.png)
## Description

What this package should be used for.

## Install this package
### Developer

Create a virtual environment and do
pip install -r requirements.txt

### As normal python package

## To Do
*[] - Refactor code to PEP8
*[] - Something else
*[x] - Test