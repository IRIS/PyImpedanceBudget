import numpy as np
from scipy import constants as cst

class Machine:
    """ This class contains all the machine parameters necessary for transverse stability.
    Together with the class ImpWake, it is an attribute of the whole impedance model (class Model)"""

    def __init__(self, parameters_file):
        """ This constructor assignes parameters to an element of class Machine, taking them from a .dat file """
        file = open(parameters_file, 'r')
        lines = file.readlines()
        file.close
        for i in range(len(lines)):
            param_name, param_value = lines[i].split()
            if param_name == 'E_eV':
                self._E_eV = float(param_value)  # energy in electron-volts
            if param_name == 'V':
                self._V = float(param_value)  # rf voltage
            if param_name == 'Qx':
                self._Qx = float(param_value)  # transverse tune in x
            if param_name == 'Qy':
                self._Qy = float(param_value)  # transverse tune in y
            if param_name == 'gamma_t':
                self._gamma_t = float(param_value)  # transition gamma
            if param_name == 'sigma_z':
                self._sigma_z = float(param_value)  # one sigma (not full) bunch length
            if param_name == 'f_rf':
                self._f_rf = float(param_value)  # rf frequency
            if param_name == 'h':
                self._h = int(param_value)  # rf harmonic number
            if param_name == 'phi_0':
                self._phi_0 = float(param_value)  # rf phase
            if param_name == 'M_symmetric_fill':
                self._M_symmetric_fill = int(param_value)  # number of equidistantly spaced bunches in the ring
            if param_name == 'Nb':
                self._Nb = float(param_value)  # number of particles per bunch


        self._f_rev = self._f_rf / self._h  # revolution frequency around the ring
        self._Omega0 = 2 * np.pi * self._f_rev  # angular revolution frequency around the ring
        self._circumference = cst.c / self._f_rev  # ring circumference
        self._gamma = self._E_eV / (cst.value('proton mass energy equivalent in MeV') * 1e6)  # relativistic gamma
        self._eta = self._gamma_t ** (-2) - self._gamma ** (-2)  # slip factor
        self._Qs = np.sqrt(self._V * self._h * np.absolute(self._eta * np.cos(self._phi_0)) / (2 * np.pi * self._E_eV))  # synchrotron tune
        self._tau_b = 4 * self._sigma_z / cst.c  # full (4 sigma) bunch length in seconds
        self._betax_avg = self._circumference / (2 * np.pi * self._Qx)  # smooth approximation average beta fucntion in x
        self._betay_avg = self._circumference / (2 * np.pi * self._Qy)  # smooth approximation average beta fucntion in y