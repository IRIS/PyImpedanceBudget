# -*- coding: utf-8 -*-

import numpy as np
from scipy import constants as cst
import sys
import os

class Model:
    """ This class is a complete impedance model, and contains elements of classes Machine and ImpWake as attributes.
    For each machine energy, there needs to be a separate element of this class. """

    def __init__(self, machine, impwake):
        self._machine = machine
        self._impwake = impwake
        print('Impedance model created')


    def weightImpedanceWithBetaFunctions(self, local_beta_x, local_beta_y):
        """ This function multiplies impedance in x and y by the ratios of the corresponding local beta functions to
         the smooth approximation average betas. """
        self._impwake.multiplyImpedance(['x'], local_beta_x / self._machine._betax_avg)
        self._impwake.multiplyImpedance(['y'], local_beta_y / self._machine._betay_avg)


    def findEffectiveImpedanceTCBI(self, plane_scan=['x', 'y'], shape='gaussian'):
        if shape == 'gaussian':
            for plane in plane_scan:
                if plane == 'x':
                    tune = self._machine._Qx
                    freqs = self._impwake._f_x
                    Zdip_weighted = self._impwake._Zxdip
                elif plane == 'y':
                    tune = self._machine._Qy
                    freqs = self._impwake._f_y
                    Zdip_weighted = self._impwake._Zydip
                m_mode = 0
                q_mode = 0
                n_mode = -(np.floor(tune) + 1)
                Qp = 0
                ReZeffTCBI = np.real(self.sum_impedance_over_Sacherer_lines(m_mode, q_mode, n_mode, Qp, freqs,
                                                                            Zdip_weighted, tune, shape,
                                                                            M=self._machine._M_symmetric_fill))
                h_sum = self.get_h_sum(m_mode, n_mode, Qp, tune, shape, M=self._machine._M_symmetric_fill)
                nturns = (-cst.e ** 2 * self._machine._Nb / (
                        2 * cst.m_p * self._machine._gamma * tune * self._machine._Omega0 * cst.c *
                        self._machine._tau_b) / (abs(m_mode) + 1) / h_sum * ReZeffTCBI) ** (-1)
                print('Re(Zeff_TCBI) at ', self._impwake._energy_string, ' in ', plane,
                      '-plane is %.2f MOhm/m which corresponds to %.2f turns rise time' % (ReZeffTCBI / 1e6, nturns))
        else:
            # in this definition of effective impedance, h(omega) comes in the summation, and the resulting
            #  impedance only makes sense if h(omega=0) is normalized to 1. This normalization is currently only
            #  done for the gaussian shape.
            sys.exit('This bunch shape is not defined for findEffectiveImpedanceTCBI!')


    def findThresholdEffectiveImpedanceTCBI(self, damper_nturns, plane_scan=['x', 'y'], shape='gaussian'):
        """ Same as function findEffectiveImpedanceTCBI, but the other way: finding impedance for which
                         the growth rate reaches the feedback damping rate. """
        if shape == 'gaussian':
            for plane in plane_scan:
                if plane == 'x':
                    tune = self._machine._Qx
                elif plane == 'y':
                    tune = self._machine._Qy
                m_mode = 0
                n_mode = -(np.floor(tune)+1)
                Qp = 0
                h_sum = self.get_h_sum(m_mode, n_mode, Qp, tune, shape, M=self._machine._M_symmetric_fill)
                MaxReZeffTCBI = 1 / (damper_nturns * (-1) * cst.e ** 2 * self._machine._Nb /
                                     (2 * cst.m_p * self._machine._gamma * tune * self._machine._Omega0 * cst.c *
                                      self._machine._tau_b) / (abs(m_mode) + 1) / h_sum)
                print('Threshold Re(Zeff_TCBI) at ', self._impwake._energy_string, ' in ', plane,
                      '-plane is %.2f MOhm/m. \n Assuming a safety factor of 3, max allowed Re(Zeff_TCBI) is %.2f MOhm/m'
                      % (MaxReZeffTCBI / 1e6, MaxReZeffTCBI / 1e6 / 3))
        else:
            # in this definition of effective impedance, h(omega) comes in the summation, and the resulting
            #  impedance only makes sense if h(omega=0) is normalized to 1. This normalization is currently only
            #  done for the gaussian shape.
            sys.exit('This bunch shape is not defined for findThresholdEffectiveImpedanceTCBI!')


    def findEffectiveImpedanceTMCI(self, plane_scan=['x', 'y'], shape='gaussian'):
        for plane in plane_scan:
            if plane == 'x':
                tune = self._machine._Qx
                freqs = self._impwake._f_x
                Zdip_weighted = self._impwake._Zxdip
            elif plane == 'y':
                tune = self._machine._Qy
                freqs = self._impwake._f_y
                Zdip_weighted = self._impwake._Zydip
            m_mode = 0
            q_mode = 0
            Qp = 0
            n_mode = 0
            Z_sum = self.sum_impedance_over_Sacherer_lines(m_mode, q_mode, n_mode, Qp, freqs, Zdip_weighted,
                                                           tune, shape, M=1)
            h_sum = self.get_h_sum(m_mode, n_mode, Qp, tune, shape, M=1)
            ImZeffTMCI = np.imag(Z_sum / h_sum)
            alpha = 0.65 # correction factor to account for the nonlinear dependence of tune with Nb in the proximity of Nth
            Nth = alpha * (4 * np.pi * cst.m_p * self._machine._gamma * tune * self._machine._Omega0 * cst.c * self._machine._tau_b * self._machine._Qs) \
                  / (cst.e ** 2 * ImZeffTMCI)
            print('Im(Zeff_TMCI) at ', self._impwake._energy_string, ' in ', plane,
                  '-plane is %.2f MOhm/m which corresponds to intensity threshold of %.2e' % (ImZeffTMCI / 1e6, Nth))


    def findThresholdEffectiveImpedanceTMCI(self, plane_scan=['x', 'y'], shape='gaussian'):
        """ Same as function findThresholdEffectiveImpedanceTMCI, but the other way: finding impedance for which
                 the threshold Nb reaches nominal Nb. """
        for plane in plane_scan:
            if plane == 'x':
                tune = self._machine._Qx
            elif plane == 'y':
                tune = self._machine._Qy
            alpha = 0.65 # correction factor to account for the nonlinear dependence of tune with Nb in the proximity of Nth
            MaxImZeffTMCI = alpha * (4 * np.pi * cst.m_p * self._machine._gamma * tune * self._machine._Omega0 *
                                     cst.c * self._machine._tau_b * self._machine._Qs) / \
                            (self._machine._Nb * cst.e ** 2)
            print('Threshold Im(Zeff_TMCI) at ', self._impwake._energy_string, ' in ', plane,
                  '-plane is %.2f MOhm/m. \n Assuming a safety factor of 3, max allowed Im(Zeff_TMCI) is %.2f MOhm/m'
                  % (MaxImZeffTMCI / 1e6, MaxImZeffTMCI / 1e6 / 3))


    def findLongEffectiveImpedance_Shaposhnikova(self, shape='gaussian'):
        """
        This function is based on the formalism from
        'Longitudinal intensity effects in the CERN Large Hadron Collider' - PhD thesis by Juan Federico ESTEBAN MÜLLER.
        Eq. (1.93) tells that Im(Z_long)/n should be limited to avoid loss of Landau damping, however it assumes a
        constant Im(Z_long)/n over the entire frequency range of interest. According to Elena Shaposhnikova, in case
        of an arbitrary impedance function, the limit should be placed on the EFFECTIVE Im(Z_long)/n, defined as the
        sum in Eq. (1.76) divided by the sum in Eq. (1.77). This function computes this (Im(Z_long)/n)_eff.
        """
        if shape == 'gaussian':
            # find frequency lines over which to do the summation
            kmax = np.floor(1 / self._machine._tau_b / self._machine._f_rev * 5)  # 5 times beyond the roll-off frequency
            k_array = np.arange(1, kmax + 1)
            omega_k_array = k_array * self._machine._Omega0

            # find impedance at these frequency lines
            ImZ_at_omega_k_array = np.imag(self.interpolateImpedance(self._impwake._f_z, self._impwake._Zlong,
                                                                     omega_k_array / (2 * np.pi)))

            # find Lambda (longitudinal beam spectrum) at these frequency lines
            BeamSpectrum_Lambda = np.exp(-0.5*(omega_k_array * self._machine._tau_b /4) ** 2)

            # find the sum in Eq. (1.76)
            TopSum = np.sum(k_array * ImZ_at_omega_k_array * BeamSpectrum_Lambda)

            # find the sum in Eq. (1.77)
            BottomSum = np.sum(k_array ** 2 * BeamSpectrum_Lambda)

            # find effective impedance by dividing the sums
            ImZeff_over_n = TopSum / BottomSum
            print('Im(Zeff/n) defined by Elena Shaposhnikova (LossOfLandau) at ', self._impwake._energy_string,
                  ' is %.2f mOhm' % (ImZeff_over_n / 1e-3))
        else:
            sys.exit('This bunch shape is not defined for findLongEffectiveImpedance_Shaposhnikova!')


    def findLongEffectiveImpedance_Ruggiero(self, shape='gaussian'):
        """ This function is based on a note
        'SINGLE-BEAM COLLECTIVE EFFECTS IN THE LHC' by FRANCESCO RUGGIERO, 1995.
        First equation on page 97 puts a limit on (Im(Z_long)/n)_eff from the longitudinal microwave instbility. The
        effective impedance is defined in the first equation on page 85 for a given azimutal head-tail number
        (denoted there as n). The most critical is mode 1 (as stated on page 96) - effective impedance for this mode is
        calculated in this function. """
        if shape == 'gaussian':
            # find frequency lines over which to do the summation
            kmax = np.floor(1 / self._machine._tau_b / self._machine._f_rev * 5)  # 5 times beyond the roll-off frequency
            k_array = np.arange(1, kmax + 1)
            omega_k_array = k_array * self._machine._Omega0

            # find impedance at these frequency lines
            ImZ_at_omega_k_array = np.imag(self.interpolateImpedance(self._impwake._f_z, self._impwake._Zlong,
                                                                     omega_k_array / (2 * np.pi)))

            # calculate the effective impedance from the first equation on page 85
            m_mode = 1  # most critical azimutal head-tail number
            q_mode = 1  # radial number - does not matter as long as distribution is gaussian
            TopSum = np.sum(ImZ_at_omega_k_array / k_array * self.modePowerSpectrum_h(omega_k_array, m_mode, q_mode, shape))
            BottomSum = np.sum(self.modePowerSpectrum_h(omega_k_array, m_mode, q_mode, shape))
            ImZeff_over_n = TopSum / BottomSum
            print('Im(Zeff/n) defined by Ruggiero at ', self._impwake._energy_string,
                  ' is %.2f mOhm' % (ImZeff_over_n / 1e-3))
        else:
            sys.exit('This bunch shape is not defined for findLongEffectiveImpedance_Ruggiero!')


    def findThresholdLongEffectiveImpedance_Ruggiero(self, shape='gaussian'):
        """ Same as function findLongEffectiveImpedance_Ruggiero, but the other way: finding impedance for which
         the threshold Nb reaches nominal Nb. """
        if shape == 'gaussian':
            MaxImZeff_over_n = 3 / 2 * self._machine._h * self._machine._V / \
                               (self._machine._Nb * cst.e *self._machine._f_rev) * \
                               (self._machine._tau_b * cst.c / self._machine._circumference) ** 3
            print('Threshold Im(Zeff_over_n) defined by Ruggiero at ', self._impwake._energy_string,
                  ' is %.2f mOhm. \n Assuming a safety factor of 3, max allowed Im(Zeff_over_n) is %.2f MOhm/m'
                  % (MaxImZeff_over_n / 1e-3, MaxImZeff_over_n / 1e-3 / 3))
        else:
            sys.exit('This bunch shape is not defined for findLongEffectiveImpedance_Ruggiero!')


    def singleBunchScan(self, plane_scan=['x', 'y'], shape='gaussian', m_scan=[0], Qp_scan=[0]):
        for plane in plane_scan:
            if plane == 'x':
                tune = self._machine._Qx
                freqs = self._impwake._f_x
                Zdip_weighted = self._impwake._Zxdip
            elif plane == 'y':
                tune = self._machine._Qy
                freqs = self._impwake._f_y
                Zdip_weighted = self._impwake._Zydip
            for m_mode in m_scan:
                q_mode = m_mode
                print('Starting single-bunch chroma scan for ', self._impwake._energy_string, ', ', plane, '-plane, m =', m_mode, ', q =',
                      q_mode)
                RedQ = np.zeros(len(Qp_scan))
                ImdQ = np.zeros(len(Qp_scan))
                for ind_Qp, Qp in enumerate(Qp_scan):
                    n_mode = 0
                    Z_sum = self.sum_impedance_over_Sacherer_lines(m_mode, q_mode, n_mode, Qp, freqs, Zdip_weighted,
                                                                   tune, shape, M=1)
                    h_sum = self.get_h_sum(m_mode, n_mode, Qp, tune, shape, M=1)
                    Tuneshift = 1j * cst.e ** 2 * self._machine._Nb / \
                                ((abs(m_mode) + 1) * 4 * np.pi * self._machine._gamma * cst.m_p * tune *
                                 self._machine._Omega0 * cst.c * self._machine._tau_b) * Z_sum / h_sum
                    RedQ[ind_Qp] = np.real(Tuneshift)
                    ImdQ[ind_Qp] = np.imag(Tuneshift)
                self.saveChromaScan_SB(plane, m_mode, Qp_scan, RedQ, ImdQ)


    def coupledBunchScan(self, plane_scan=['x', 'y'], shape='gaussian', m_scan=[0], Qp_scan=[0]):
        for plane in plane_scan:
            if plane == 'x':
                tune = self._machine._Qx
                freqs = self._impwake._f_x
                Zdip_weighted = self._impwake._Zxdip
            elif plane == 'y':
                tune = self._machine._Qy
                freqs = self._impwake._f_y
                Zdip_weighted = self._impwake._Zydip
            for m_mode in m_scan:
                q_mode = m_mode
                print('Starting coupled bunch chroma scan for ', self._impwake._energy_string, ', ', plane, '-plane, m =', m_mode, ', q =',
                      q_mode)
                MostUnstableRedQ = np.zeros(len(Qp_scan))
                MostUnstableImdQ = np.zeros(len(Qp_scan))
                MostUnstable_ncb = np.zeros(len(Qp_scan), dtype='int')
                MostDownShiftedRedQ = np.zeros(len(Qp_scan))
                MostDownShiftedImdQ = np.zeros(len(Qp_scan))
                MostDownShifted_ncb = np.zeros(len(Qp_scan), dtype='int')
                LeastDownShiftedRedQ = np.zeros(len(Qp_scan))
                LeastDownShiftedImdQ = np.zeros(len(Qp_scan))
                LeastDownShifted_ncb = np.zeros(len(Qp_scan), dtype='int')
                for ind_Qp, Qp in enumerate(Qp_scan):
                    Tuneshift_array = np.zeros(self._machine._M_symmetric_fill, 'complex')
                    for n_cb in range(self._machine._M_symmetric_fill):  # scanning n_cb from 0 to M-1
                        Z_sum = self.sum_impedance_over_Sacherer_lines(m_mode, q_mode, n_cb, Qp, freqs, Zdip_weighted,
                                                                       tune, shape, M=self._machine._M_symmetric_fill)
                        h_sum = self.get_h_sum(m_mode, n_cb, Qp, tune, shape, M=self._machine._M_symmetric_fill)
                        Tuneshift_array[n_cb] = 1j * cst.e ** 2 * self._machine._Nb / \
                                                ((abs(m_mode) + 1) * 4 * np.pi * self._machine._gamma * cst.m_p * tune *
                                                 self._machine._Omega0 * cst.c * self._machine._tau_b) * Z_sum / h_sum
                    Growthrate_array = -np.imag(Tuneshift_array) * self._machine._Omega0
                    ind_MostUnstableGrowthrate = np.argmax(Growthrate_array)
                    MostUnstable_ncb[ind_Qp] = ind_MostUnstableGrowthrate
                    MostUnstableRedQ[ind_Qp] = np.real(Tuneshift_array[ind_MostUnstableGrowthrate])
                    MostUnstableImdQ[ind_Qp] = np.imag(Tuneshift_array[ind_MostUnstableGrowthrate])

                    DownShift_array = -np.real(Tuneshift_array)
                    ind_MaxDownShift = np.argmax(DownShift_array)
                    MostDownShifted_ncb[ind_Qp] = ind_MaxDownShift
                    MostDownShiftedRedQ[ind_Qp] = np.real(Tuneshift_array[ind_MaxDownShift])
                    MostDownShiftedImdQ[ind_Qp] = np.imag(Tuneshift_array[ind_MaxDownShift])
                    ind_MinDownShift = np.argmin(DownShift_array)
                    LeastDownShifted_ncb[ind_Qp] = ind_MinDownShift
                    LeastDownShiftedRedQ[ind_Qp] = np.real(Tuneshift_array[ind_MinDownShift])
                    LeastDownShiftedImdQ[ind_Qp] = np.imag(Tuneshift_array[ind_MinDownShift])
                self.saveChromaScan_CB(plane, m_mode, Qp_scan, MostUnstable_ncb, MostUnstableRedQ,
                                       MostUnstableImdQ, MostDownShifted_ncb, MostDownShiftedRedQ, MostDownShiftedImdQ,
                                       LeastDownShifted_ncb, LeastDownShiftedRedQ, LeastDownShiftedImdQ)


    def saveChromaScan_SB(self, plane, m_mode, Qp_scan, RedQ, ImdQ):
        """save data to file: real and imaginary tuneshifts in the single bunch regime as functions of chroma"""
        folder = self._impwake._scenariofolder + 'Chroma scans/' + self._impwake._energy_string + '_' + plane + '-plane/'
        if not os.path.exists(folder):
            os.makedirs(folder)
        file = open(folder + 'm=' + str(m_mode) + ', single bunch mode.dat', 'w')
        file.write('Qp\tRe(dQ)\tIm(dQ)\n')
        for ind_Qp, Qp in enumerate(Qp_scan):
            file.write(str.format("{0:.2f}", Qp_scan[ind_Qp]) + '\t' + str.format("{0:.5e}", RedQ[ind_Qp]) + '\t'
                       + str.format("{0:.5e}", ImdQ[ind_Qp]) + '\n')
        file.close()


    def saveChromaScan_CB(self, plane, m_mode, Qp_scan, MostUnstable_ncb, MostUnstableRedQ,
                          MostUnstableImdQ, MostDownShifted_ncb, MostDownShiftedRedQ, MostDownShiftedImdQ,
                          LeastDownShifted_ncb, LeastDownShiftedRedQ, LeastDownShiftedImdQ):
        """save data to two files:
        1) real and imaginary tuneshifts in of the most unstable coupled bunch mode as functions of chroma
        2) for the coupled bunch modes with the highest and the lowest downward shifts of Re(dQ), save both Re(dQ)
         and Im(dQ), as as a function of chroma. Needed to analyzed the so-called multibunch TMCI (if it even exists)"""
        folder = self._impwake._scenariofolder + 'Chroma scans/' + self._impwake._energy_string + '_' + plane + '-plane/'
        if not os.path.exists(folder):
            os.makedirs(folder)
        file = open(folder + 'm=' + str(m_mode) + ', most unstable CB mode.dat', 'w')
        file.write('Qp\tMostUnstable_ncb\tMostUnstableRe(dQ)\tMostUnstableIm(dQ)\n')
        for ind_Qp, Qp in enumerate(Qp_scan):
            file.write(str.format("{0:.2f}", Qp_scan[ind_Qp]) + '\t' + str(MostUnstable_ncb[ind_Qp]) + '\t' +
                       str.format("{0:.5e}", MostUnstableRedQ[ind_Qp]) + '\t'
                       + str.format("{0:.5e}", MostUnstableImdQ[ind_Qp]) + '\n')
        file.close()

        file = open(folder + 'm=' + str(m_mode) + ', most and least downshifted CB modes.dat', 'w')
        file.write('Qp\tMostDownShifted_ncb\tMostDownShiftedRe(dQ)\tMostDownShiftedIm(dQ)\tLeastDownShifted_ncb\tLeastDownShiftedRe(dQ)\tLeastDownShiftedIm(dQ)\n')
        for ind_Qp, Qp in enumerate(Qp_scan):
            file.write(str.format("{0:.2f}", Qp_scan[ind_Qp]) + '\t' + str(MostDownShifted_ncb[ind_Qp]) + '\t' +
                       str.format("{0:.5e}", MostDownShiftedRedQ[ind_Qp]) + '\t' +
                       str.format("{0:.5e}", MostDownShiftedImdQ[ind_Qp]) + '\t' + str(LeastDownShifted_ncb[ind_Qp]) +
                       '\t' + str.format("{0:.5e}", LeastDownShiftedRedQ[ind_Qp]) + '\t' +
                       str.format("{0:.5e}", LeastDownShiftedImdQ[ind_Qp]) + '\n')
        file.close()


    def modePowerSpectrum_h(self, omega_array, m_mode, q_mode, shape):
        """this function returns mode power spectrum h(omega) defined in Chao eq. 6.143 for gaussian shape and in OVERVIEW
         OF SINGLE-BEAM COHERENT INSTABILITIES IN CIRCULAR ACCELERATORS by E. Me'tral eq. 3-7 for parabolic shape"""
        if shape == 'gaussian':
            h_array = (omega_array * self._machine._tau_b / 4) ** (2 * abs(m_mode)) * \
                      np.exp(-(omega_array * self._machine._tau_b / 4) ** 2)
        elif shape == 'parabolic':
            if m_mode % 2 == 0 and q_mode % 2 == 0:
                F_array = (-1) ** ((abs(m_mode) + abs(q_mode)) / 2) * \
                          np.cos(omega_array * self._machine._tau_b / 2) ** 2
            elif m_mode % 2 ==0 and q_mode % 2 == 1:
                F_array = (-1) ** ((abs(m_mode) + abs(q_mode) + 3) / 2) / (2 * 1j) * \
                          np.sin(omega_array * self._machine._tau_b)
            elif m_mode % 2 == 1 and q_mode % 2 == 0:
                F_array = (-1) ** ((abs(m_mode) + abs(q_mode) + 1) / 2) / (2 * 1j) * \
                          np.sin(omega_array * self._machine._tau_b)
            else:
                F_array = (-1) ** ((abs(m_mode) + abs(q_mode) + 2) / 2) * np.sin(omega_array * self._machine._tau_b / 2) ** 2
            h_array = self._machine._tau_b ** 2 / np.pi ** 4 * (abs(m_mode) + 1) * (abs(q_mode) + 1) * F_array * \
                      ((omega_array * self._machine._tau_b / np.pi) ** 2 - (abs(m_mode) + 1) ** 2) ** (-1) * \
                      ((omega_array * self._machine._tau_b / np.pi) ** 2 - (abs(q_mode) + 1) ** 2) ** (-1)
        else:
            sys.exit('This bunch shape is not defined for the power spectrum h(omega)!')
        return h_array


    def get_h_sum(self, m_mode, n_mode, Qp, tune, shape, M):
        """this function computes the sum of the h(omega) over Sacherer's frequency lines as used in the denominator of
         the effective impedance. M can be either 1 (for single bunch) or M_symmetric_fill (for coupled bunch). """
        omega_k_array = self.sachererFrequencyLines(n_mode, tune, m_mode, Qp, M)
        omega_xi = self._machine._Omega0 * Qp / self._machine._eta
        q_mode = m_mode
        h_sum = np.sum(self.modePowerSpectrum_h(omega_k_array - omega_xi, m_mode, q_mode, shape))
        return h_sum


    def sum_impedance_over_Sacherer_lines(self, m_mode, q_mode, n_mode, Qp, freqs, Zdip_weighted, tune, shape, M):
        """ This function computes the sum of Z over Sacherer's frequency lines as used in the numenator of the
        effective impedance. M can be either 1 (for single bunch) or M_symmetric_fill (for coupled bunch). """
        omega_k_array = self.sachererFrequencyLines(n_mode, tune, m_mode, Qp, M)
        omega_xi = self._machine._Omega0 * Qp / self._machine._eta
        needed_freq_array = omega_k_array / (2 * np.pi)
        interpolated_Z = self.interpolateImpedance(freqs, Zdip_weighted, np.abs(needed_freq_array))
        Z_at_omega_k_array = np.sign(omega_k_array) * np.real(interpolated_Z) + 1j * np.imag(interpolated_Z)
        Z_sum = np.sum(Z_at_omega_k_array * self.modePowerSpectrum_h(omega_k_array - omega_xi, m_mode, q_mode, shape))
        return Z_sum


    def sachererFrequencyLines(self, n_mode, tune, m_mode, Qp, M):
        """this function returns frequency lines used in Sacherer's formula as written in OVERVIEW OF SINGLE-BEAM COHERENT
         INSTABILITIES IN CIRCULAR ACCELERATORS by E. Me'tral. M can be either 1 (for single bunch) or
          M_symmetric_fill (for coupled bunch)."""
        HowManyTimesOverRolloffFreqToGo = {'m_mode': [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                                           'value': [2.25, 3.7, 5, 5.6, 7.2, 7.7, 9.0, 9.6, 10.1, 10.6,
                                                     11.2]}  # where h falls by 3 orders of magnitude for each m_mode
        if m_mode in HowManyTimesOverRolloffFreqToGo['m_mode']:
            ind = HowManyTimesOverRolloffFreqToGo['m_mode'].index(m_mode)  # index of m_mode in the dictionary
            factor = HowManyTimesOverRolloffFreqToGo['value'][ind]  # value that corresponds to that index
            frev = self._machine._Omega0 / (2 * np.pi)
            kmax = np.floor(factor / (self._machine._tau_b * M * frev))

            # the region around 0 frequency
            k_array_around_zero = np.arange(-kmax, kmax + 1)

            # the region around chromatic frequency
            omega_xi = self._machine._Omega0 * Qp / self._machine._eta
            k_xi = np.floor(omega_xi / (M * self._machine._Omega0))
            k_array_around_chromatic_freq = np.arange(-kmax, kmax + 1) + k_xi

            k_array = np.append(k_array_around_zero, k_array_around_chromatic_freq)
            k_array = np.unique(k_array)
            omega_k_array = ((n_mode + k_array * M + tune + m_mode * self._machine._Qs) * self._machine._Omega0)
        else:
            sys.exit('Passed head-tail number was not defined in function sachererFrequencyLines... yet')
        return omega_k_array


    def interpolateImpedance(self, freqs, Z, needed_freq_array):
        """this function does linear interpolation, used if values of impedance are needed at points that are not in
         the impedance file"""
        if freqs[0] > min(needed_freq_array):
            sys.exit('Impedance has to be supplied at lower frequency points')
        elif freqs[-1] <= max(needed_freq_array):
            sys.exit('Impedance has to be supplied at higher frequency points')
        else:
            interpolated_Z = np.interp(needed_freq_array, freqs, Z)
            return interpolated_Z