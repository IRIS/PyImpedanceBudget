import numpy as np
from scipy import constants as cst
from matplotlib import pyplot as plt
import pandas as pd
import os
import sys

class ImpWake:
    """ This class contains all the operation that can be done with impedance and wake.
    Together with the class Machine, it is an attribute of the whole impedance model (class Model)"""

    def __init__(self, scenariofolder, energy_string):
        self._scenariofolder = scenariofolder
        self._energy_string = energy_string
        self._f_z = np.empty(0)
        self._Zlong = np.empty(0)
        self._f_x = np.empty(0)
        self._Zxdip = np.empty(0)
        self._f_y = np.empty(0)
        self._Zydip = np.empty(0)
        self._s_z = np.empty(0)
        self._Wlong = np.empty(0)
        self._s_x = np.empty(0)
        self._Wxdip = np.empty(0)
        self._s_y = np.empty(0)
        self._Wydip = np.empty(0)


    def setImpedanceFromFile(self, filename, plane, delimitertype, n_skiprows):
        """ this function sets impedance in only one plane from any file named in any way"""
        if plane == 'long':
            data = pd.read_csv(filename, names=['col1', 'col2', 'col3'], delimiter=delimitertype, skiprows=n_skiprows)
            self._f_z = np.array(data['col1'])
            self._Zlong = np.array(data['col2']) + 1j * np.array(data['col3'])
            print('Longitudinal impedance set from file', filename)
        elif plane == 'x':
            data = pd.read_csv(filename, names=['col1', 'col2', 'col3'], delimiter=delimitertype, skiprows=n_skiprows)
            self._f_x = np.array(data['col1'])
            self._Zxdip = np.array(data['col2']) + 1j * np.array(data['col3'])
            print('Horizontal impedance set from file', filename)
        elif plane == 'y':
            data = pd.read_csv(filename, names=['col1', 'col2', 'col3'], delimiter=delimitertype, skiprows=n_skiprows)
            self._f_y = np.array(data['col1'])
            self._Zydip = np.array(data['col2']) + 1j * np.array(data['col3'])
            print('Vertical impedance set from file', filename)


    def setImpedanceFromFolder(self, folder, plane_scan):
        """ this function sets impedances from the supplied folder, assuming they are named as Zxdip_50TeV.dat, etc"""
        for plane in plane_scan:
            if plane == 'long':
                self.setImpedanceFromFile(folder + 'Zlong_' + self._energy_string + '.dat', 'long', '\t', 1)
                print('Longitudinal impedance set from folder', folder)
            elif plane == 'x':
                self.setImpedanceFromFile(folder + 'Zxdip_' + self._energy_string + '.dat', 'x', '\t', 1)
                print('Horizontal impedance set from folder', folder)
            elif plane == 'y':
                self.setImpedanceFromFile(folder + 'Zydip_' + self._energy_string + '.dat', 'y', '\t', 1)
                print('Vertical impedance set from folder', folder)


    def setLocalImpedance(self, plane_scan):
        """ this function sets impedances from folder assigned to the class, assuming they are named as Zxdip_50TeV.dat, etc"""
        self.setImpedanceFromFolder(self._scenariofolder+'Impedance and wake data/', plane_scan)


    def addToImpedance(self, plane, added_f, added_Z):
        if plane == 'long':
            if self._f_z.size == 0:  # if freq and impedance have not been initialized yet
                self._f_z = added_f
                self._Zlong = added_Z
            else:
                combined_f = np.append(self._f_z, added_f)
                combined_f = np.unique(combined_f)  # sort frequencies in unique way
                combined_Z = np.interp(combined_f, self._f_z, self._Zlong) + np.interp(combined_f, added_f, added_Z)
                self._f_z = combined_f
                self._Zlong = combined_Z
            print('Contribution is added to longitudinal impedance')
        elif plane == 'x':
            if self._f_x.size == 0:  # if freq and impedance have not been initialized yet
                self._f_x = added_f
                self._Zxdip = added_Z
            else:
                combined_f = np.append(self._f_x, added_f)
                combined_f = np.unique(combined_f)  # sort frequencies in unique way
                combined_Z = np.interp(combined_f, self._f_x, self._Zxdip) + np.interp(combined_f, added_f, added_Z)
                self._f_x = combined_f
                self._Zxdip = combined_Z
            print('Contribution is added to horizontal impedance')
        elif plane == 'y':
            if self._f_y.size == 0:  # if freq and impedance have not been initialized yet
                self._f_y = added_f
                self._Zydip = added_Z
            else:
                combined_f = np.append(self._f_y, added_f)
                combined_f = np.unique(combined_f)  # sort frequencies in unique way
                combined_Z = np.interp(combined_f, self._f_y, self._Zydip) + np.interp(combined_f, added_f, added_Z)
                self._f_y = combined_f
                self._Zydip = combined_Z
            print('Contribution is added to vertical impedance')


    def addToImpedanceFromFolder(self, added_scenariofolder, plane_scan):
        """ this function adds impedances from the supplied folder, assuming they are named as Zxdip_50TeV.dat, etc"""
        for plane in plane_scan:
            if plane == 'long':
                added_Z_file = added_scenariofolder + 'Zlong_' + self._energy_string + '.dat'
                data = pd.read_csv(added_Z_file, names=['col1', 'col2', 'col3'], delimiter='\t', skiprows=1)
                added_f = np.array(data['col1'])
                added_Z = np.array(data['col2']) + 1j * np.array(data['col3'])
                self.addToImpedance('long', added_f, added_Z)
            elif plane == 'x':
                added_Z_file = added_scenariofolder + 'Zxdip_' + self._energy_string + '.dat'
                data = pd.read_csv(added_Z_file, names=['col1', 'col2', 'col3'], delimiter='\t', skiprows=1)
                added_f = np.array(data['col1'])
                added_Z = np.array(data['col2']) + 1j * np.array(data['col3'])
                self.addToImpedance('x', added_f, added_Z)
            elif plane == 'y':
                added_Z_file = added_scenariofolder + 'Zydip_' + self._energy_string + '.dat'
                data = pd.read_csv(added_Z_file, names=['col1', 'col2', 'col3'], delimiter='\t', skiprows=1)
                added_f = np.array(data['col1'])
                added_Z = np.array(data['col2']) + 1j * np.array(data['col3'])
                self.addToImpedance('y', added_f, added_Z)


    def multiplyImpedance(self, plane_scan, factor):
        for plane in plane_scan:
            if plane == 'long':
                self._Zlong = self._Zlong * factor
            elif plane == 'x':
                self._Zxdip = self._Zxdip * factor
            elif plane == 'y':
                self._Zydip = self._Zydip * factor


    def addZeroFrequencyToImpedance(self, plane_scan):
        for plane in plane_scan:
            if plane == 'long':
                if self._f_z[0] != 0:
                    self._f_z = np.append(0, self._f_z)
                    self._Zlong = np.append(np.real(self._Zlong[0]) + 1j * 0, self._Zlong)
                    print('Zero frequency is added to longitudinal impedance')
            elif plane == 'x':
                if self._f_x[0] != 0:
                    self._f_x = np.append(0, self._f_x)
                    self._Zxdip = np.append(0 + 1j * np.imag(self._Zxdip[0]), self._Zxdip)
                    print('Zero frequency is added to horizontal impedance')
            elif plane == 'y':
                if self._f_y[0] != 0:
                    self._f_y = np.append(0, self._f_y)
                    self._Zydip = np.append(0 + 1j * np.imag(self._Zydip[0]), self._Zydip)
                    print('Zero frequency is added to vertical impedance')


    def convertImpedanceToWake(self, plane_scan, AreHOMsIncluded=True, relevant_fmin=1e2,
                               relevant_fmax=1e10, error=0.1, n_resolved_periods=100):

        # pick frequency and impedance depending on the plane
        for plane in plane_scan:
            if plane == 'long':
                sys.exit('Finding longitudinal wake for resistive-wall-like impedance is tricky and gives '
                         'noise - to do in the future')
                f = self._f_z
                Z = self._Zlong
                planeString = 'long'
                wakeString = 'Wlong[V/C]'
            elif plane == 'x':
                f = self._f_x
                Z = self._Zxdip
                planeString = 'xdip'
                wakeString = 'Wxdip[V/(C*m)]'
            elif plane == 'y':
                f = self._f_y
                Z = self._Zydip
                planeString = 'ydip'
                wakeString = 'Wydip[V/(C*m)]'

            # assign distance points s for which wake will be calculated
            smin = cst.c * error ** 2 / 16 / relevant_fmax  # starting distance sufficient to give impedance at relevant_fmax with an accuracy error, if wake is converted back to impedance
            smax = cst.c * 1 / np.pi ** 2 / error ** 2 / relevant_fmin  # finishing distance sufficient to give impedance at relevant_fmin with an accuracy error, if wake is converted back to impedance
            s_array_without_HOMs = np.logspace(np.log10(smin), np.log10(smax), 2000)  # "2000 points should be enough for everyone"
            if AreHOMsIncluded == True:
                HOM_filename = self._scenariofolder + 'HOM_list/HOM_list_' + planeString + '_' + self._energy_string + '.dat'
                data = pd.read_csv(HOM_filename, names=['col1', 'col2', 'col3'], delimiter='\t', skiprows=1)
                f_HOM = np.array(data['col1'])
                Q_HOM = np.array(data['col3'])
                if max(Q_HOM) > n_resolved_periods:
                    print('Warning: only ', n_resolved_periods, ' oscillations of HOMs are resolved, while max(Q_HOM)=', max(Q_HOM))
                HOM_step = 1 / 20 * min(1 / f_HOM) * cst.c # step in distance small enough to resolve HOM oscillation (20 points per period)
                HOM_duration = 1 * max(n_resolved_periods / f_HOM) * cst.c # duration in distance long enough to resolve HOMs until they decay
                s_array_HOMs = np.arange(HOM_step, HOM_duration, HOM_step) # distance array to resolve HOMs
                s_array = np.append(s_array_without_HOMs, s_array_HOMs)
                s_array = np.sort(s_array)
            else:
                s_array = s_array_without_HOMs

            # calculate wake
            print('Calculating ', len(s_array), ' wake points')
            W = np.zeros(len(s_array))
            for ind_f in range(len(f)-1):
                if plane == 'long':  # we assume that ReZ = A + (f - fn) / (fn + 1 - fn) * (B - A), ImZ = C + (f - fn) / (fn + 1 - fn) * (D - C)
                    A = np.real(Z[ind_f])
                    B = np.real(Z[ind_f + 1])
                    C = np.imag(Z[ind_f])
                    D = np.imag(Z[ind_f + 1])
                else:  #we assume that ImZ=A+(f-fn)/(fn+1-fn)*(B-A), ReZ=C+(f-fn)/(fn+1-fn)*(D-C)
                    A = np.imag(Z[ind_f])
                    B = np.imag(Z[ind_f + 1])
                    C = np.real(Z[ind_f])
                    D = np.real(Z[ind_f + 1])
                E_array = 2 * np.pi * s_array / cst.c
                f1 = f[ind_f]
                f2 = f[ind_f + 1]
                Term1_at_f2 = (E_array * B * (f2 - f1) * np.sin(E_array * f2) + (B - A) * np.cos(E_array * f2))
                Term1_at_f1 = (E_array * A * (f2 - f1) * np.sin(E_array * f1) + (B - A) * np.cos(E_array * f1))
                Term2_at_f2 = (-E_array * D * (f2 - f1) * np.cos(E_array * f2) + (D - C) * np.sin(E_array * f2))
                Term2_at_f1 = (-E_array * C * (f2 - f1) * np.cos(E_array * f1) + (D - C) * np.sin(E_array * f1))
                if plane == 'long':
                    dSum = 2 * E_array ** (-2) * (f2 - f1) ** (-1) * (Term1_at_f2 - Term2_at_f2 - (Term1_at_f1 - Term2_at_f1))
                else:
                    dSum = 2 * E_array ** (-2) * (f2 - f1) ** (-1) * (Term1_at_f2 + Term2_at_f2 - (Term1_at_f1 + Term2_at_f1))
                W = W + dSum

            # save wake as a class attribute
            if plane == 'long':
                self._s_z = s_array
                self._Wlong = W
            elif plane == 'x':
                self._s_x = s_array
                self._Wxdip = W
            elif plane == 'y':
                self._s_y = s_array
                self._Wydip = W

            # save wake to file
            folder = self._scenariofolder + 'Impedance and wake data/'
            if not os.path.exists(folder):
                os.makedirs(folder)
            file = open(folder + 'W' + planeString + '_' + self._energy_string + '.dat', 'w')
            file.write('s[m]\t' + wakeString + '\n')
            for ind_s, s in enumerate(s_array):
                file.write(str.format("{0:.5e}", s) + '\t' + str.format("{0:.5e}", W[ind_s]) + '\n')
            file.close()

    def convertWakeToImpedance(self, plane, relevant_fmin=1e2, relevant_fmax=1e10):
        """ This function serves as a check that the wake is computed properly. For that, impedance is restored back
         from wake to make sure it matches the original impedance in the frequency range of relevance. """

        # pick distance and wake depending on the plane
        if plane == 'long':
            s = self._s_z
            W = self._Wlong
        elif plane == 'x':
            s = self._s_x
            W = self._Wxdip
        elif plane == 'y':
            s = self._s_y
            W = self._Wydip

        # calculate impedance restored from wake
        f_array = np.logspace(np.log10(relevant_fmin), np.log10(relevant_fmax), 2000)
        omega_array = 2 * np.pi * f_array
        Z = np.zeros(len(f_array), 'complex')

        # use relations: Zlong=1/c int_0^inf Wlong(s) exp(-i omega s/c) ds,
        # Zperp=i/c int_0^inf Wperp(s) exp(-i omega s/c) ds.

        # separately calculate the integral for every interval [s_n, s_n+1]
        for ind_s in range(len(s) - 1):
            s1 = s[ind_s]
            s2 = s[ind_s + 1]
            if s1 == s2:
                dSum = 0
            else:
                # first, express wake between s1 and s2 as A + B (s-s1)
                A = W[ind_s]
                B = (W[ind_s + 1] - W[ind_s]) / (s2 - s1)
                # find the integral I = int_s1^s2 (A + B (s-s1)) exp(-i omega s/c) ds = Term(s2) - Term(s1)
                Term_at_s2 = cst.c / omega_array * np.exp(-1j * omega_array * s2 / cst.c) * \
                             (1j * A - 1j * B * s1 + cst.c / omega_array * B + 1j * s2 * B)
                Term_at_s1 = cst.c / omega_array * np.exp(-1j * omega_array * s1 / cst.c) * \
                             (1j * A - 1j * B * s1 + cst.c / omega_array * B + 1j * s1 * B)
                # having obtained the integral, get impedance by multiplying by a constant
                if plane == 'long':
                    dSum = 1 / cst.c * (Term_at_s2 - Term_at_s1)
                else:
                    dSum = 1j / cst.c * (Term_at_s2 - Term_at_s1)

            Z += dSum
        return f_array, Z






    def plotImpedance(self, plane_scan, xscale='logarithmic', yscale='logarithmic', flimits=None, title='Supplied scenario'):
        for plane in plane_scan:
            if plane == 'long':
                f = self._f_z
                Z = self._Zlong
                label = r'$Re(Z_{||}), Im(Z_{||})$ [Ohm]'
                filename = 'Zlong_' + self._energy_string + '.png'
                figtitle = r'' + title + ', ' + self._energy_string + ', ' + '$Z_{||}$'
            elif plane == 'x':
                f = self._f_x
                Z = self._Zxdip
                label = r'$Re(Z_x^{dip}), Im(Z_x^{dip})$ [Ohm/m]'
                filename = 'Zxdip_' + self._energy_string + '.png'
                figtitle = r'' + title + ', ' + self._energy_string + ', ' + '$Z_x^{dip}$'
            elif plane == 'y':
                f = self._f_y
                Z = self._Zydip
                label = r'$Re(Z_y^{dip}), Im(Z_y^{dip})$ [Ohm/m]'
                filename = 'Zydip_' + self._energy_string + '.png'
                figtitle = r'' + title + ', ' + self._energy_string + ', ' + '$Z_y^{dip}$'
            if 'Z' in locals():  # if Z was assigned, then plot
                fig = plt.figure(1, figsize=(22, 16))
                if xscale == 'linear' and yscale == 'linear':
                    line1, = plt.plot(f, np.real(Z), '-r', label='real part')
                    line2, = plt.plot(f, np.imag(Z), '-b', label='imaginary part')
                elif xscale == 'linear' and yscale == 'logarithmic':
                    line1, = plt.semilogy(f, np.real(Z), '-r', label='real part')
                    line2, = plt.semilogy(f, np.imag(Z), '-b', label='imaginary part')
                elif xscale == 'logarithmic' and yscale == 'linear':
                    line1, = plt.semilogx(f, np.real(Z), '-r', label='real part')
                    line2, = plt.semilogx(f, np.imag(Z), '-b', label='imaginary part')
                elif xscale == 'logarithmic' and yscale == 'logarithmic':
                    line1, = plt.loglog(f, np.real(Z), '-r', label='real part')
                    line2, = plt.loglog(f, np.imag(Z), '-b', label='imaginary part')
                if flimits is not None:
                    plt.xlim(flimits)
                    i = np.where((f >= flimits[0]) & (f < flimits[1]))[0]
                    ymin = np.min([np.min(np.real(Z[i])), np.min(np.imag(Z[i]))])
                    ymax = np.max([np.max(np.real(Z[i])), np.max(np.imag(Z[i]))])
                    plt.ylim(ymin - 0.1 * np.abs(ymin), ymax + 0.1 * np.abs(ymax))
                ax = plt.gca()
                tx = ax.get_xaxis().get_offset_text()
                tx.set_size(25)
                ty = ax.get_yaxis().get_offset_text()
                ty.set_size(25)
                plt.legend(handles=[line1, line2], fontsize=25)
                plt.ylabel(label, fontsize=25)
                plt.xlabel('Frequency [Hz]', fontsize=25)
                plt.tick_params(labelsize=25)
                plt.title(figtitle, fontsize=25)
                plt.grid(True, which='both')

                # save figure to file
                folder = self._scenariofolder + 'Impedance and wake pics/'
                if not os.path.exists(folder):
                    os.makedirs(folder)
                plt.savefig(folder + filename, dpi = 300)
                plt.close(fig)
                print('Impedance figure saved')


    def plotWake(self, plane_scan, xscale='logarithmic', yscale='logarithmic', slimits=None, title='Supplied scenario'):
        for plane in plane_scan:
            if plane == 'long':
                s = self._s_z
                W = self._Wlong
                label = r'Longitudinal wake function $W_{||}$ [V/C]'
                filename = 'Wlong_' + self._energy_string + '.png'
                figtitle = r'' + title + ', ' + self._energy_string + ', ' + '$W_{||}$'
            elif plane == 'x':
                s = self._s_x
                W = self._Wxdip
                label = r'Transverse wake function $W_x^{dip}$ [V/(C*m)]'
                filename = 'Wxdip_' + self._energy_string + '.png'
                figtitle = r'' + title + ', ' + self._energy_string + ', ' + '$W_x^{dip}$'
            elif plane == 'y':
                s = self._s_y
                W = self._Wydip
                label = r'Transverse wake function $W_y^{dip}$ [V/(C*m)]'
                filename = 'Wydip_' + self._energy_string + '.png'
                figtitle = r'' + title + ', ' + self._energy_string + ', ' + '$W_y^{dip}$'
            if 'W' in locals():  # if W was assigned, then plot
                fig = plt.figure(1, figsize=(22, 16))
                if xscale == 'linear' and yscale == 'linear':
                    plt.plot(s, W, '-r')
                elif xscale == 'linear' and yscale == 'logarithmic':
                    plt.semilogy(s, W, '-r')
                elif xscale == 'logarithmic' and yscale == 'linear':
                    plt.semilogx(s, W, '-r')
                elif xscale == 'logarithmic' and yscale == 'logarithmic':
                    plt.loglog(s, W, '-r')
                if slimits is not None:
                    plt.xlim(slimits)
                    i = np.where((s >= slimits[0]) & (s < slimits[1]))[0]
                    ymin = np.min([np.min(np.real(W[i])), np.min(np.imag(W[i]))])
                    ymax = np.max([np.max(np.real(W[i])), np.max(np.imag(W[i]))])
                    plt.ylim(ymin - 0.1 * np.abs(ymin), ymax + 0.1 * np.abs(ymax))
                ax = plt.gca()
                tx = ax.get_xaxis().get_offset_text()
                tx.set_size(25)
                ty = ax.get_yaxis().get_offset_text()
                ty.set_size(25)
                plt.ylabel(label, fontsize=25)
                plt.xlabel('Distance behind the bunch $s$ [m]', fontsize=25)
                plt.tick_params(labelsize=25)
                plt.title(figtitle, fontsize=25)
                plt.grid(True, which='both')

                # save figure to file
                folder = self._scenariofolder + 'Impedance and wake pics/'
                if not os.path.exists(folder):
                    os.makedirs(folder)
                plt.savefig(folder + filename)
                plt.close(fig)
                print('Wake figure saved')


    def saveImpedance(self, plane_scan):
        """save data to file: real and imaginary impedance as function of frequency"""
        for plane in plane_scan:
            if plane == 'long':
                freqs = self._f_z
                Z = self._Zlong
                filename = 'Zlong_' + self._energy_string + '.dat'
                header = 'Frequency[Hz]\tRe_Zlong[Ohm]\tIm_Zlong[Ohm]\n'
            elif plane == 'x':
                freqs = self._f_x
                Z = self._Zxdip
                filename = 'Zxdip_' + self._energy_string + '.dat'
                header = 'Frequency[Hz]\tRe_Zxdip[Ohm/m]\tIm_Zxdip[Ohm/m]\n'
            elif plane == 'y':
                freqs = self._f_y
                Z = self._Zydip
                filename = 'Zydip_' + self._energy_string + '.dat'
                header = 'Frequency[Hz]\tRe_Zydip[Ohm/m]\tIm_Zydip[Ohm/m]\n'

            folder = self._scenariofolder + 'Impedance and wake data/'
            if not os.path.exists(folder):
                os.makedirs(folder)
            file = open(folder + filename, 'w')
            file.write(header)
            for ind_f, f in enumerate(freqs):
                file.write(str.format("{0:.7e}", f) + '\t' + str.format("{0:.5e}", np.real(Z[ind_f])) + '\t'
                           + str.format("{0:.5e}", np.imag(Z[ind_f])) + '\n')
            file.close()
            print('Impedance in', plane, 'plane is saved')


    def addCollimatorGeomImpedance(self, collimator_file, delta, l, width, b_pipe, Q, betax_avg, betay_avg):
        # first, read collimator file
        data = pd.read_csv(collimator_file, names=['#', 'ID name', 'angle[rad]', 'betax[m]', 'betay[m]', 'halfgap[m]', 'Material', 'Length[m]', 'sigx[m]', 'sigy[m]', 'tilt1[rad]', 'tilt2[rad]', 'nsig'], delimiter=r"\s+", skiprows=1)
        angle = np.array(data['angle[rad]'])
        betax = np.array(data['betax[m]'])
        betay = np.array(data['betay[m]'])
        halfgap = np.array(data['halfgap[m]'])
        Npeak = 500
        Nbroadband = 500
        f_res = 0.3 * cst.c / b_pipe
        Rw_total_x = 0
        Rw_total_y = 0
        Rw_long_total = 0
        Z0 = cst.value('characteristic impedance of vacuum')
        for ind in range(len(angle)):
            # longitudinal
            RoverQ_long_single_taper = 4 * cst.mu_0 * f_res / 2 * 0.43 * delta / l * delta
            Rw_long_single_taper = RoverQ_long_single_taper * Q
            Rw_long_single_coll = 2 * Rw_long_single_taper
            Rw_long_total = Rw_long_total + Rw_long_single_coll

            # transverse
            RoverQ_trans_single_taper_NonTaperedDimension = Z0 * np.pi / 4 * delta / l * (
                        1 / halfgap[ind] - 1 / (halfgap[ind] + delta)) / np.pi ** 2
            RoverQ_trans_single_taper_TaperedDimension = Z0 * np.pi / 4 * width * delta / l * (
                        1 / halfgap[ind] ** 2 - 1 / (halfgap[ind] + delta) ** 2) / (2 * np.pi)
            Rw_trans_single_taper_NonTaperedDimension = RoverQ_trans_single_taper_NonTaperedDimension * Q
            Rw_trans_single_taper_TaperedDimension = RoverQ_trans_single_taper_TaperedDimension * Q
            Rw_trans_single_coll_NonTaperedDimension = 2 * Rw_trans_single_taper_NonTaperedDimension
            Rw_trans_single_coll_TaperedDimension = 2 * Rw_trans_single_taper_TaperedDimension
            Rw_x_single_coll = betax[ind] / betax_avg * (Rw_trans_single_coll_NonTaperedDimension * np.sin(
                angle[ind]) ** 2 + Rw_trans_single_coll_TaperedDimension * np.cos(angle[ind]) ** 2)
            Rw_y_single_coll = betay[ind] / betay_avg * (Rw_trans_single_coll_NonTaperedDimension * np.cos(
                angle[ind]) ** 2 + Rw_trans_single_coll_TaperedDimension * np.sin(angle[ind]) ** 2)

            # update total shunts
            Rw_total_x += Rw_x_single_coll
            Rw_total_y += Rw_y_single_coll

        self.addResonatorImpedance('long', f_res, Rw_long_total, Q, Npeak, Nbroadband)
        self.addResonatorImpedance('x', f_res, Rw_total_x, Q, Npeak, Nbroadband)
        self.addResonatorImpedance('y', f_res, Rw_total_y, Q, Npeak, Nbroadband)


    def addResonatorImpedance(self, plane, f_res, Rw, Q, Npeak, Nbroadband):

        # define broadband frequency lines
        f_broadband = np.logspace(0, 12, Nbroadband)

        # define resonance frequency lines
        def Zpp_long(f):  # second derivative of the longitudinal peak shape for efficient sampling
            Zpp = -(((1 / f_res + f_res / f ** 2) * 1j * Q * Rw) / (1 + (f / f_res - f_res / f) * 1j * Q) ** 2)
            return Zpp

        def Zpp_trans(f):  # second derivative of the transverse peak shape for efficient sampling
            Zpp = (2 * f_res * (1 / f_res + f_res / f ** 2) ** 2 * 1j * Q ** 2 * Rw) / (f * (1 + (f / f_res - f_res / f) * 1j * Q) ** 3) \
                  + (2 * f_res ** 2 * 1j * Q * Rw) / (f ** 4 * (1 + (f / f_res - f_res / f) * 1j * Q) ** 2) \
                  + (2 * f_res * (1 / f_res + f_res / f ** 2) * 1j * Q * Rw) / (f ** 2 * (1 + (f / f_res - f_res / f) * 1j * Q) ** 2) \
                  + (2 * f_res * Rw) / (f ** 3 * (1 + (f / f_res - f_res / f) * 1j * Q))
            return Zpp


        def Ztr(f):
            Ztr = f_res / f * Rw / (1 + 1j * Q * (f / f_res - f_res / f))
            return Ztr

        # iteratively add new frequency points, spaced tighter where second derivative of Z is high
        f_peak = [f_res]
        df = f_res / Q
        default_step = df / Npeak
        # find f_broadband step around the frequency of interest to know when to stop increasing f_peak step
        idx = (np.abs(f_broadband - f_peak)).argmin()
        f_broadband_step_around_f_peak = f_broadband[idx + 1] - f_broadband[idx]


        for iteration in range(Npeak):
            if plane == 'long':
                step = default_step * np.abs(Zpp_long(f_res)) / np.abs(Zpp_long(f_peak[0]))  # do larger steps as we move away from the peak
            elif plane == 'x' or plane == 'y':
                step = default_step * np.abs(Zpp_trans(f_res)) / np.abs(Zpp_trans(f_peak[0]))  # do larger steps as we move away from the peak
            if step < f_broadband_step_around_f_peak:
                f_peak = np.append(f_peak[0] - step, f_peak)
                f_peak = np.append(f_peak, f_peak[-1] + step)
        f_peak = f_peak[f_peak >= 0]  # leave only positive values of frequency


        # remove broadband frequency lines from the region already covered by resonant lines
        f_broadband_left = f_broadband[f_broadband <= f_peak[0]]
        f_broadband_right = f_broadband[f_broadband >= f_peak[-1]]
        f_broadband = np.append(f_broadband_left, f_broadband_right)

        # join frequency lines
        f = np.append(f_broadband, f_peak)
        f = np.unique(f)

        # calculate impedance
        if plane == 'long':
            Z_res = Rw / (1 + 1j * Q * (f / f_res - f_res / f))
            self.addToImpedance('long', added_f=f, added_Z=Z_res)
        elif plane == 'x':
            Z_res = f_res / f * Rw / (1 + 1j * Q * (f / f_res - f_res / f))
            self.addToImpedance('x', added_f=f, added_Z=Z_res)
        elif plane == 'y':
            Z_res = f_res / f * Rw / (1 + 1j * Q * (f / f_res - f_res / f))
            self.addToImpedance('y', added_f=f, added_Z=Z_res)