from setuptools import setup

setup(name="PyImpedanceBudget",
      version="0.1",
      description='Handle accelerator wake/impedance files and scans. ',
      author="Sergey Arsenyev",
      author_email="sergey.arsenyev@cern.ch",
      packages=["PyImpedanceBudget"],
      zip_safe=False,
      install_requires=[
            "numpy",
            "pandas",
            "scipy",
            "matplotlib",
      ])